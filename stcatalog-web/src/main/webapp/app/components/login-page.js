import Ember from 'ember';

export default Ember.Component.extend({


  authManager: Ember.inject.service('session'),
  sessionAccount:Ember.inject.service('sessionAccount'),

  actions: {


    authenticate() {
      const { login, password } = this.getProperties('login', 'password');

        this.get('authManager').authenticate('authenticator:oauth2', login, password).catch(()=>{
          alert("User or Password Incorect");
          }).then( ()=>{
                /*jshint -W030 */
                Ember.set(this.get('authManager'),'username', login),
                  Ember.set(this.get('authManager'), 'password', password)

        });

    },

    invalidateSession() {
            this.get('authManager').invalidate();
        }
  }
});
