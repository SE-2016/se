import Ember from 'ember';

export default Ember.Route.extend({
	setupController: function (controller, model) {
    this._super(controller, model);
    controller.set('model', model);
  },
  session: Ember.inject.service('session'),
	model() {
		var teacherId=this.get('session.data.authenticated.teacher.id');
    	var query =this.store.query('optional-discipline', {'id_teacher': teacherId});
    	return query;
	}
});
