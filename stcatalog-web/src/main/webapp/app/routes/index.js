import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Ember.Route.extend(ApplicationRouteMixin,{
	authManager: Ember.inject.service('session'),
    setupController (controller, model) {
        console.log(this.get('authManager.isAuthenticated'));
        this._super(controller, model);
    },
    actions: {
        invalidateSession: function() {
            this.get('authManager').invalidate();
        }
    }
});

