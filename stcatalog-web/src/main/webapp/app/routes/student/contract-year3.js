import Ember from 'ember';

export default Ember.Route.extend({
  setupController: function (controller, model) {
    this._super(controller, model);
    controller.set('model', model);
  },
  session: Ember.inject.service('session'),

  model() {
    return Ember.RSVP.hash({
      year: this.get('session.data.authenticated.student.year'),
      optionalDisciplines: this.store.query('optional-discipline', {
        'student_id': this.get('session.data.authenticated.student.id'),
        'year': '3'
      }),
      disciplines: this.store.query('discipline', {
        'student_id': this.get('session.data.authenticated.student.id'),
        'year': '3'
      }),
      failed: this.store.query('discipline', {
        'student_id': this.get('session.data.authenticated.student.id'),
        'failed': 'yes'
      }),
    });
  }
});
