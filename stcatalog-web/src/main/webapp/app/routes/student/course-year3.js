import Ember from 'ember';

export default Ember.Route.extend({
  session: Ember.inject.service('session'),
  model() {
	var studentId=this.get('session.data.authenticated.student.id');
    return this.store.query('discipline',{'student_id':studentId, 'year':'3'});
	}
});
