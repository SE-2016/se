import Ember from 'ember';

export default Ember.Route.extend({
	actions:{
		setToF:function(){
			var myselect = document.getElementById("selectOpt1");

     		var TOF=myselect.options[myselect.selectedIndex].value;

      function failure(reason) {
        console.log(reason);
      }

     		Ember.$.ajax({
          		data: {
        			TOF:TOF
          		},
          		method: 'POST',
          		url: '/api/setToF',
          		succes:function(){
            		alert("Added with success");
          		},
          		error:function(reason){
            		failure(reason);
          		}
        	});
		}
	}
});
