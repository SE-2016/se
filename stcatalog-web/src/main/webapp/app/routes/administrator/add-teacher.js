import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    onAddTeacherButtonClick: function () {

      function failure(reason) {
        console.log(reason);
      }

      var firstName = document.getElementById("basic-addon1");
      var lastName = document.getElementById("basic-addon2");
      var cnp = document.getElementById("basic-addon3");
      var rank = document.getElementById("basic-addon4");
      var department = document.getElementById("basic-addon5");
      var username = document.getElementById("basic-addon6");
      var password = document.getElementById("basic-addon7");


      Ember.$.ajax({
          data: {
            firstName: firstName.value,
            lastName: lastName.value,
            cnp: cnp.value,
            rank: rank.value,
            department: department.value,
            username: username.value,
            password: password.value
          },
          method: 'POST',
          url: '/api/onAddTeachers',
          succes:function(){
            alert("Added with success");
          },
          error:function(reason){
            failure(reason);
          }
        });

      firstName.value = '';
      lastName.value = '';
      cnp.value='';
      rank.value = '';
      department.value ='';
      username.value = '';
      password.value = '';
    }
  }

});
