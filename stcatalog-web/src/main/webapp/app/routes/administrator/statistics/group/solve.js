import Ember from 'ember';

export default Ember.Route.extend({
    queryParams: {
    spec: {
      refreshModel: true
    },
    studyLevel: {
      refreshModel: true
    },
    studyLine: {
      refreshModel: true
    },
    promotion: {
      refreshModel: true
    },
  },
  model(params){
    var spec = params.spec;
    var studyLevel = params.studyLevel;
    var studyLine = params.studyLine;
    var promotion = params.promotion;
    var group = params.group;
    alert(spec);

    var query = this.store.query('student-grade', {
      spec: spec,
      studyLevel: studyLevel,
      studyLine: studyLine,
      promotion: promotion,
      group: group
    });
    return query;


  }

});
