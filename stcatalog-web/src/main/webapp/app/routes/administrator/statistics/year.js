import Ember from 'ember';

export default Ember.Route.extend({
  setupController: function(controller, model){
   controller.set('model', model);
  },
  actions: {
    onStatisticsButtonClick: function () {

      var myselect = document.getElementById("selectOpt1");
      var spec=myselect.options[myselect.selectedIndex].value;
      myselect = document.getElementById("selectOpt2");
      var studyLevel1=myselect.options[myselect.selectedIndex].value;

      myselect = document.getElementById("selectOpt3");
      var studyLine1=myselect.options[myselect.selectedIndex].value;
      var promotion = document.getElementById("basic-addon1").value;
      var model=this.modelFor('administrator.statistics.year.solve');
      
      this.transitionTo('administrator.statistics.year.solve',{queryParams: {spec: spec,studyLevel:studyLevel1,studyLine:studyLine1,promotion:promotion }});

    }
  }

});
