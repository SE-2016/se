import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    onStatisticsButtonClick: function () {

      var myselect = document.getElementById("selectOpt1");
      var spec=myselect.options[myselect.selectedIndex].value;
      myselect = document.getElementById("selectOpt2");
      var studyLevel1=myselect.options[myselect.selectedIndex].value;

      myselect = document.getElementById("selectOpt3");
      var studyLine1=myselect.options[myselect.selectedIndex].value;

      var promotion = document.getElementById("basic-addon1").value;
      var group = document.getElementById("basic-addon2").value;

      this.transitionTo('administrator.statistics.group.solve',{queryParams: {spec: spec,studyLevel:studyLevel1,studyLine:studyLine1,promotion:promotion,group:group }});

    }
  }

});
