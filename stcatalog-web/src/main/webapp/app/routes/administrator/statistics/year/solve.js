import Ember from 'ember';



export default Ember.Route.extend({
  queryParams: {
    spec: {
      refreshModel: true
    },
    studyLevel: {
      refreshModel: true
    },
    studyLine: {
      refreshModel: true
    },
    promotion: {
      refreshModel: true
    },
  },
  model:function(params){

    var spec = params.spec;
    var studyLevel = params.studyLevel;
    var studyLine = params.studyLine;
    var promotion = params.promotion;


    //var studyLevel = this.get('studyLevel');
    //var studyLine = this.get('studyLine');

    var query = this.store.query('student-grade', {
      spec: spec,
      studyLevel: studyLevel,
      studyLine: studyLine,
      promotion: promotion
    });

    return query;
    //return this.store.findRecord('student', spec,studyLevel);
    //return $.getJSON("/students/" + spec+"/"+studyLine +"/"+ studyLevel );

    /*return this.store.findAll('student');
     var spec = params.spec;
     var studyLevel = params.studyLevel;
     var studyLine = params.studyLine;*/
    //return this.store.findAll('student');
    //var query = this.store.query('student', {specialization: spec} );
    //return query;
  },
  setupController: function(controller, model) {
    // this only gets called on initial load and *not* on refresh
    this._super(controller, model);

    // do stuff that should happen on refresh
  },

  actions: {
    refreshRoute: function() {
      this.refresh();
    }
  }
});
