import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    onAddStudentButtonClick: function () {

      function failure(reason) {
        console.log(reason);
      }

      var firstName = document.getElementById("basic-addon1");
      var lastName = document.getElementById("basic-addon2");
      var cnp = document.getElementById("div_special");
      var serialNumber = document.getElementById("basic-addon3");

      //specialisation
      var myselect = document.getElementById("selectOpt1");
      var spec=myselect.options[myselect.selectedIndex].value;
      //study Level
      myselect = document.getElementById("selectOpt2");
      var studyLevel1=myselect.options[myselect.selectedIndex].value;
      //Study Line
      myselect = document.getElementById("selectOpt3");
      var studyLine1=myselect.options[myselect.selectedIndex].value;
      //var specialization = document.getElementById("basic-addon4");
      //var studyLevel = document.getElementById("basic-addon5");
      //var studyLine = document.getElementById("basic-addon6");
      var semester = document.getElementById("basic-addon7");
      var group = document.getElementById("basic-addon8");
      var username = document.getElementById("basic-addon9");
      var password = document.getElementById("basic-addon10");

      /*var onAddStudent = this.store.createRecord('OnAddStudent', {
        firstName: firstName.value,
        lastName: lastName.value,
        serialNumber: serialNumber.value,
        specialization: specialization.value,
        studyLevel: studyLevel.value,
        studyLine: studyLine.value,
        semester: semester.value,
        group: group.value,
        username: username.value,
        password: password.value
      });
      onAddStudent.save().then(alert("Added with success")).catch(failure);*/

      Ember.$.ajax({
          data: {
            firstName: firstName.value,
            lastName: lastName.value,
            cnp:cnp.value,
            serialNumber: serialNumber.value,
            specialization: spec,
            studyLevel: studyLevel1,
            studyLine: studyLine1,
            semester: semester.value,
            group: group.value,
            username: username.value,
            password: password.value
          },
          method: 'POST',
          url: '/api/onAddStudents',
          succes:function(){
            alert("Added with success");
            firstName.value = '';
            lastName.value = '';
            serialNumber.value='';
            semester.value = '';
            group.value = '';
            username.value = '';
            password.value = '';
            Ember.$('#selectOpt2').empty();
            Ember.$('#selectOpt3').empty();
          },
          error:function(reason){
            failure(reason);
          }
        });


    }
  }

});
