import DS from 'ember-data';

export default DS.Model.extend({
  	disciplinegroup:DS.attr(),
  	priority:DS.attr(),
  	name:DS.attr(),
  	cod:DS.attr(),
  	teacher_id:DS.attr(),
  	department:DS.attr(),
  	teacher:DS.attr(),
  	semester:DS.attr()
});
