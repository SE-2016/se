import DS from 'ember-data';

export default DS.Model.extend({
  firstName: DS.attr(),
  lastName: DS.attr(),
  serialNumber:DS.attr(),
  specialization: DS.attr(),
  studyLevel: DS.attr(),
  studyLine: DS.attr(),
  semester: DS.attr(),
  group: DS.attr(),
  username: DS.attr(),
  password: DS.attr()
});
