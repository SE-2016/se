import DS from 'ember-data';


export default DS.Model.extend({
  firstName: DS.attr(),
  lastName: DS.attr(),
  cnp: DS.attr(),
  serialNumber: DS.attr(),
  grade: DS.attr()
});
