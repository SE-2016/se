import DS from 'ember-data';

export default DS.Model.extend({
  	disciplinegroup:DS.attr(),
  	priority:DS.attr(),
  	Name:DS.attr(),
  	Cod:DS.attr(),
  	teacher_id:DS.attr(),
  	department_id:DS.attr()
});
