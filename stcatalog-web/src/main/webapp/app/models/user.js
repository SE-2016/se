import DS from 'ember-data';
import Ember from 'ember';
export default DS.Model.extend({
  	firstName: DS.attr(),
  	toString: Ember.computed('id', 'firstName', function() {
    return `${this.get('id')} ${this.get('firstName')} `;
  })
});
