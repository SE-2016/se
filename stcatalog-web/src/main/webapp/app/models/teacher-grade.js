import DS from 'ember-data';

export default DS.Model.extend({
  cnp: DS.attr(),
  firstName: DS.attr(),
  lastName: DS.attr(),
  rank: DS.attr(),
  grade: DS.attr()
});
