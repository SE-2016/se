import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['spec', 'studyLevel', 'studyLine','promotion'],
  spec : '',
  studyLevel : '',
  studyLine : '',
  promotion : ''
});
