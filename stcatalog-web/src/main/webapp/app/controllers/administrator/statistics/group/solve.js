import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['spec', 'studyLevel', 'studyLine','promotion','group'],
  spec : '',
  studyLevel : '',
  studyLine : '',
  promotion : '',
  group : ''

});
