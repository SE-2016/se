import Ember from 'ember';

export default Ember.Controller.extend({
  actions:{
    changeSelect1(){
      var myselect = document.getElementById("selectOpt1");
      var selector=myselect.options[myselect.selectedIndex].value;
      if(selector==='Informatica'){
        Ember.$('#selectOpt2').empty();
        Ember.$('#selectOpt3').empty();
        Ember.$('#selectOpt2').append(Ember.$('<option>', {
          value: '',
          text: 'Study Level',
          selected:true,
          disabled:true,
          style:'display:none'
        }));
        Ember.$('#selectOpt2').append(Ember.$('<option>', {
          value: 'Licenta',
          text: 'Licenta'
        }));
        Ember.$('#selectOpt2').append(Ember.$('<option>', {
          value: 'Master',
          text: 'Master'
        }));
      }
      if(selector==='Matematica'){
        Ember.$('#selectOpt2').empty();
        Ember.$('#selectOpt3').empty();
        Ember.$('#selectOpt2').append(Ember.$('<option>', {
          value: '',
          text: 'Study Level',
          selected:true,
          disabled:true,
          style:'display:none'
        }));
        Ember.$('#selectOpt2').append(Ember.$('<option>', {
          value: 'Licenta',
          text: 'Licenta'
        }));
        Ember.$('#selectOpt2').append(Ember.$('<option>', {
          value: 'Master',
          text: 'Master'
        }));
      }
    },
    changeSelect2(){
      var myselect = document.getElementById("selectOpt2");
      var selector=myselect.options[myselect.selectedIndex].value;
      if(selector==='Licenta'){
        Ember.$('#selectOpt3').empty();
        Ember.$('#selectOpt3').append(Ember.$('<option>', {
          value: '',
          text: 'Study Line',
          selected:true,
          disabled:true,
          style:'display:none'
        }));
        Ember.$('#selectOpt3').append(Ember.$('<option>', {
          value: 'Engleza',
          text: 'Engleza'
        }));
        Ember.$('#selectOpt3').append(Ember.$('<option>', {
          value: 'Romana',
          text: 'Romana'
        }));
      }
      if(selector==='Master'){
        Ember.$('#selectOpt3').empty();
        Ember.$('#selectOpt3').append(Ember.$('<option>', {
          value: '',
          text: 'Study Line',
          selected:true,
          disabled:true,
          style:'display:none'
        }));
        Ember.$('#selectOpt3').append(Ember.$('<option>', {
          value: 'Engleza',
          text: 'Engleza'
        }));
        Ember.$('#selectOpt3').append(Ember.$('<option>', {
          value: 'Romana',
          text: 'Romana'
        }));
      }
    },
    changeSelect3(){

    }
  }
});
