import Ember from 'ember';

const { service } = Ember.inject;

export default Ember.Controller.extend({
  session: service('session'),
  actions: {
    invalidateSession: function() {
      this.get('session').invalidate();
    }
  }
});
