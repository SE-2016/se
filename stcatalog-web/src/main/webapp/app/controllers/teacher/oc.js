import Ember from 'ember';

export default Ember.Controller.extend({
	session: Ember.inject.service('session'),
	actions:{
		AddOptionalDiscipline() {
	      	var self = this;

			var teacherId=self.get('session.data.authenticated.teacher.id');
			var name = document.getElementById("basic-addon1");
			var cod = document.getElementById("basic-addon2");
			//specialisation
			var myselect = document.getElementById("selectOpt1");
		  	var spec=myselect.options[myselect.selectedIndex].value;
		  	//study Level
		  	myselect = document.getElementById("selectOpt2");
		  	var studyLevel1=myselect.options[myselect.selectedIndex].value;
		  	//Study Line
		  	myselect = document.getElementById("selectOpt3");
		  	var studyLine1=myselect.options[myselect.selectedIndex].value;

      		//var studyLevel = document.getElementById("basic-addon4");
      		//var studyLine = document.getElementById("basic-addon5");


      		var semester = document.getElementById("basic-addon6");
			//optionalDiscipline.save().then().catch(failure);
			var name1=name.value;
			var cod1=cod.value ;
			var sem=semester.value;
			Ember.$.ajax({
	        data: {
	          name: name1,
	          cod: cod1,
	          teacher_id: teacherId,
	          specialization:spec,
	          studyLevel:studyLevel1,
	          studyLine:studyLine1,
	          semester:sem
	        },
	        method: 'POST',
	        url: '/api/optionalDisciplines',
	        succes:function(){
	        	alert("Added with success");
	        },
	        error:function(XMLHttpRequest, textStatus, errorThrown){
	        	alert(errorThrown+"You have already suggested 2 courses");
          },

	      });
      name.value = '';
      cod.value = '';
      semester.value = '';
      Ember.$('#selectOpt2').empty();
      Ember.$('#selectOpt3').empty();

		},
		changeSelect1(){
			var myselect = document.getElementById("selectOpt1");
		  	var selector=myselect.options[myselect.selectedIndex].value;
		  	if(selector==='Informatica'){
		  		Ember.$('#selectOpt2').empty();
		  		Ember.$('#selectOpt3').empty();
		  		Ember.$('#selectOpt2').append(Ember.$('<option>', {
    				value: '',
    				text: 'Study Level',
    				selected:true,
    				disabled:true,
    				style:'display:none'
				}));
		  		Ember.$('#selectOpt2').append(Ember.$('<option>', {
            value: 'Licenta',
            text: 'Licenta'
				}));
				Ember.$('#selectOpt2').append(Ember.$('<option>', {
    				value: 'Master',
    				text: 'Master'
				}));
		  	}
		  	if(selector==='Matematica'){
		  		Ember.$('#selectOpt2').empty();
		  		Ember.$('#selectOpt3').empty();
		  		Ember.$('#selectOpt2').append(Ember.$('<option>', {
    				value: '',
    				text: 'Study Level',
    				selected:true,
    				disabled:true,
    				style:'display:none'
				}));
		  		Ember.$('#selectOpt2').append(Ember.$('<option>', {
            value: 'Licenta',
            text: 'Licenta'
				}));
				Ember.$('#selectOpt2').append(Ember.$('<option>', {
    				value: 'Master',
    				text: 'Master'
				}));
		  	}
		},
		changeSelect2(){
			var myselect = document.getElementById("selectOpt2");
		  	var selector=myselect.options[myselect.selectedIndex].value;
      if (selector === 'Licenta') {
		  		Ember.$('#selectOpt3').empty();
		  		Ember.$('#selectOpt3').append(Ember.$('<option>', {
    				value: '',
    				text: 'Study Line',
    				selected:true,
    				disabled:true,
    				style:'display:none'
				}));
		  		Ember.$('#selectOpt3').append(Ember.$('<option>', {
    				value: 'Engleza',
    				text: 'Engleza'
				}));
				Ember.$('#selectOpt3').append(Ember.$('<option>', {
    				value: 'Romana',
    				text: 'Romana'
				}));
		  	}
		  	if(selector==='Master'){
		  		Ember.$('#selectOpt3').empty();
		  		Ember.$('#selectOpt3').append(Ember.$('<option>', {
    				value: '',
    				text: 'Study Line',
    				selected:true,
    				disabled:true,
    				style:'display:none'
				}));
		  		Ember.$('#selectOpt3').append(Ember.$('<option>', {
    				value: 'Engleza',
    				text: 'Engleza'
				}));
				Ember.$('#selectOpt3').append(Ember.$('<option>', {
    				value: 'Romana',
    				text: 'Romana'
				}));
		  	}
		},
		changeSelect3(){

		}
	}
});
