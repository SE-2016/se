import Ember from 'ember';

export default Ember.Controller.extend({
	sortProperties: ['name'],
    sortAscending: false,

    actions:{

        sortBy: function(property) {
            this.set('sortProperties', [property]);
            this.toggleProperty('sortAscending');
            model: Ember.computed.sort('disciplines', 'sortProperties');
        } 
    }
});
