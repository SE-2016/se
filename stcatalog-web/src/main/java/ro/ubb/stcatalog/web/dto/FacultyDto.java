package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.Faculty;

import java.util.List;


public class FacultyDto {
    private List<Faculty> faculties;

    public FacultyDto() {
    }

    public FacultyDto(List<Faculty> faculties) {
        this.faculties = faculties;
    }

    public List<Faculty> getFaculties() {
        return faculties;
    }

    public void setFaculties(List<Faculty> faculties) {
        this.faculties = faculties;
    }


}
