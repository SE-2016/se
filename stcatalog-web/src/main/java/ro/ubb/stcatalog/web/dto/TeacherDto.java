package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.person.Teacher;


public class TeacherDto {
    private long id;
    private String firstName;
    private String lastName;
    private String cnp;
    private String rank;

    public TeacherDto() {
    }

    public TeacherDto(String firstName, String lastName, String cnp, String rank) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cnp = cnp;
        this.rank = rank;
    }

    public TeacherDto(Teacher teacher) {
        this.id = teacher.getId();
        this.firstName = teacher.getFirstName();
        this.lastName = teacher.getLastName();
        this.cnp = teacher.getCnp();
        this.rank = teacher.getRank();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
