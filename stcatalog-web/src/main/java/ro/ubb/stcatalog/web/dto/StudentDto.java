package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.person.Student;


public class StudentDto {
    private String firstName;
    private String lastName;
    private String serialNumber;
    private int group;

    public StudentDto() {
    }

    public StudentDto(String firstName, String lastName, String serialNumber, int group) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.serialNumber = serialNumber;
        this.group = group;
    }

    public StudentDto(Student student) {
        this.firstName = student.getFirstName();
        this.lastName = student.getLastName();
        this.serialNumber = student.getSerialNumber();
        this.group = student.getGroup().getNumber();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }
}
