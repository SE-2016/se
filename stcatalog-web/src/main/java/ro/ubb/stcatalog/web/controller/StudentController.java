package ro.ubb.stcatalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ubb.stcatalog.core.model.Contract_OptDiscipline;
import ro.ubb.stcatalog.core.model.Discipline;
import ro.ubb.stcatalog.core.model.OptionalDiscipline;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.service.Interfaces.StudentService;
import ro.ubb.stcatalog.web.dto.DisciplinesDto;
import ro.ubb.stcatalog.web.dto.OptionalDisciplineDto;
import ro.ubb.stcatalog.web.dto.OptionalDisciplinesDto;
import ro.ubb.stcatalog.web.dto.StudentsDto;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

@RestController
public class StudentController {

    private static final Logger log = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/students", method = RequestMethod.GET, produces = "application/vnd.api+json")
    public
    @ResponseBody
    StudentsDto getStudents() {
        log.trace("getStudents");

        List<Student> students = studentService.findAll();

        log.trace("getStudents: students={}", students);

        return new StudentsDto(students);
    }


    @RequestMapping(value = "/students/save", method = RequestMethod.GET, produces = "application/vnd.api+json")
    public
    @ResponseBody
    StudentsDto save(Student s) {
        log.trace("save");

        Student students = studentService.store(s);

        log.trace("save: students={}", students);
        List<Student> l1st=new ArrayList<>();
        l1st.add(students);
        return new StudentsDto(l1st);
    }


    @RequestMapping(value = "/disciplines", params = {"student_id", "year"}, method = RequestMethod.GET, produces = "application/vnd.api+json", consumes = "application/vnd.api+json")
    public
    @ResponseBody
    DisciplinesDto getDisciplineOfStudentByYear(@RequestParam(value = "student_id") final Long studentId, @RequestParam(value = "year") final Long year) {
        log.trace("getDsiciplines");

        List<Discipline> dsiciplines = studentService.findAllDisciplinesByYear(studentId, year);
        Collections.sort(dsiciplines, (o1, o2) -> o1.getSemester().getNumber() - o2.getSemester().getNumber());
        log.trace("getdsiciplines: dsiciplines={}", dsiciplines);

        return new DisciplinesDto(dsiciplines);
    }

    @RequestMapping(value = "/optionalDisciplines/{id}", method = RequestMethod.POST)
    public Map<String, OptionalDisciplineDto> updateOC(@PathVariable final Long id, final HttpServletRequest request)throws IOException {
        String priority = request.getParameter("priority");
        int id_student = Integer.parseInt(request.getParameter("id_student"));
        int year = Integer.parseInt(request.getParameter("year"));
        Contract_OptDiscipline contract_optDiscipline = studentService.updateOC(id, id_student, Integer.parseInt(priority), year);

        Map<String, OptionalDisciplineDto> optionalDisciplineDtoHashMap= new HashMap<>();
        optionalDisciplineDtoHashMap.put("optionalDiscipline", new OptionalDisciplineDto(contract_optDiscipline.getOptionalDiscipline()));

        return optionalDisciplineDtoHashMap;
    }

    @RequestMapping(value = "/disciplines", params = {"student_id", "failed"}, method = RequestMethod.GET, produces = "application/vnd.api+json", consumes = "application/vnd.api+json")
    public
    @ResponseBody
    DisciplinesDto getFailedDisciplineOfStudent(@RequestParam(value = "student_id") final Long studentId, @RequestParam(value = "failed") final String failed) {
        log.trace("getFailedDsiciplines");

        List<Discipline> dsiciplines = studentService.findAllDisciplinesFailed(studentId);
        Collections.sort(dsiciplines, (o1, o2) -> o1.getSemester().getNumber() - o2.getSemester().getNumber());
        log.trace("getdsiciplines: dsiciplines={}", dsiciplines);

        return new DisciplinesDto(dsiciplines);
    }

    @RequestMapping(value = "/optionalDisciplines", params = {"student_id", "year"}, method = RequestMethod.GET, produces = "application/vnd.api+json", consumes = "application/vnd.api+json")
    public
    @ResponseBody
    OptionalDisciplinesDto getOptionalDisciplinesByYear(@RequestParam(value = "student_id") final Long id, @RequestParam(value = "year") final Long year) {

        List<OptionalDiscipline> optionalDisciplines = studentService.findAOCByStudentAndYear(id, year);
        Collections.sort(optionalDisciplines, (o1, o2) -> o1.getSemester().getNumber() - o2.getSemester().getNumber());

        log.trace("getOptionalDisciplines: optionalDisciplines={}", optionalDisciplines);

        return new OptionalDisciplinesDto(optionalDisciplines);
    }
}
