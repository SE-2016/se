package ro.ubb.stcatalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ubb.stcatalog.core.model.OptionalDiscipline;
import ro.ubb.stcatalog.core.model.person.Teacher;
import ro.ubb.stcatalog.core.service.Interfaces.TeacherService;
import ro.ubb.stcatalog.web.dto.OptionalDisciplineDto;
import ro.ubb.stcatalog.web.dto.OptionalDisciplinesDto;
import ro.ubb.stcatalog.web.dto.TeacherGradeList;
import ro.ubb.stcatalog.web.dto.TeachersDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Created by radu.
 */


@RestController
public class TeacherController {

    private static final Logger log = LoggerFactory.getLogger(TeacherController.class);

    @Autowired
    private TeacherService teacherService;

    @RequestMapping(value = "/teachers", method = RequestMethod.GET, produces = "application/vnd.api+json")
    public
    @ResponseBody
    TeachersDto getTeachers() {
        log.trace("getTeachers");

        List<Teacher> teachers = teacherService.findAll();

        log.trace("getTeachers: teachers={}", teachers);

        return new TeachersDto(teachers);
    }

    @RequestMapping(value = "/teacherGrades", method = RequestMethod.GET, produces = "application/vnd.api+json")
    public
    @ResponseBody
    TeacherGradeList getTeachersWithGrades() {
        log.trace("getTeachers");

        List<Teacher> teachers = teacherService.findAllWithGrades();
        Collections.sort(teachers, new Comparator() {

            public int compare(Object o1, Object o2) {

                float x1 = ((Teacher) o1).getGrade();
                float x2 = ((Teacher) o2).getGrade();
                int sComp = Float.compare(x2, x1);

                if (sComp != 0) {
                    return sComp;
                } else {
                    String x11 = ((Teacher) o1).getFirstName();
                    String x22 = ((Teacher) o2).getFirstName();
                    return x11.compareTo(x22);
                }
            }
        });
        log.trace("getTeachers: teachers={}", teachers);

        return new TeacherGradeList(teachers);
    }

    @RequestMapping(value = "/optionalDisciplines",params = {"id_teacher"}, method = RequestMethod.GET, produces = "application/vnd.api+json", consumes = "application/vnd.api+json")
    public
    @ResponseBody
    OptionalDisciplinesDto getOptionalDisciplines(@RequestParam(value = "id_teacher") final Long id) {
        List<OptionalDiscipline> optionalDisciplines= teacherService.findAllOC(id);
        Collections.sort(optionalDisciplines, (o1, o2) -> o1.getSemester().getNumber() - o2.getSemester().getNumber());
        log.trace("getOptionalDisciplines: optionalDisciplines={}", optionalDisciplines);

        return new OptionalDisciplinesDto(optionalDisciplines);
    }

    @RequestMapping(value = "/optionalDisciplines",params = {"id_student"}, method = RequestMethod.GET, produces = "application/vnd.api+json", consumes = "application/vnd.api+json")
    public
    @ResponseBody
    OptionalDisciplinesDto getOptionalDisciplines1(@RequestParam(value = "id_student") final Long id) {

        List<OptionalDiscipline> optionalDisciplines= teacherService.findAOCByStudent(id);
        Collections.sort(optionalDisciplines, (o1, o2) -> o1.getSemester().getNumber() - o2.getSemester().getNumber());
        log.trace("getOptionalDisciplines: optionalDisciplines={}", optionalDisciplines);

        return new OptionalDisciplinesDto(optionalDisciplines);
    }

    @RequestMapping(value = "/optionalDisciplines", method = RequestMethod.POST, produces = "application/vnd.api+json")
    public void createOC(final HttpServletRequest request,
                         final HttpServletResponse response) throws IOException {
        String name1 = request.getParameter("name");
        String cod = request.getParameter("cod");
        Long id = Long.valueOf(request.getParameter("teacher_id"));
        String specialization = request.getParameter("specialization");
        String studyLevel = request.getParameter("studyLevel");
        String studyLine = request.getParameter("studyLine");
        Long semester_id = Long.valueOf(request.getParameter("semester"));

        Optional<OptionalDiscipline> oc = teacherService.createOc(name1, cod,specialization,studyLevel,studyLine, id, semester_id);
        if (oc.isPresent()) {
            Map<String, OptionalDisciplineDto> OdDtoMap = new HashMap<>();
            //OdDtoMap.put("optionalDiscipline", new OptionalDisciplineDto(oc.get()));

            response.getWriter().write("Done");
        } else
            response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Already 2 disciplines sugested");
    }

    @RequestMapping(value = "/optionalDisciplines", method = RequestMethod.PUT)
    public Map<String, OptionalDisciplineDto> updateOC(final HttpServletRequest request)throws IOException {
        String id= request.getParameter("id");
        String groupnumber = request.getParameter("groupnumber");
        OptionalDiscipline optionalDiscipline=teacherService.updateOC(Long.parseLong(id),groupnumber);

        Map<String, OptionalDisciplineDto> optionalDisciplineDtoHashMap= new HashMap<>();
        optionalDisciplineDtoHashMap.put("optionalDiscipline", new OptionalDisciplineDto(optionalDiscipline));



        return optionalDisciplineDtoHashMap;
    }
}