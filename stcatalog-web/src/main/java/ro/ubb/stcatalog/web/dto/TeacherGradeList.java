package ro.ubb.stcatalog.web.dto;


import ro.ubb.stcatalog.core.model.person.Teacher;

import java.util.ArrayList;
import java.util.List;

public class TeacherGradeList {
    private List<TeacherGradeDto> teacherGrades;

    public TeacherGradeList() {
    }

    public TeacherGradeList(List<Teacher> teachers1) {
        teacherGrades = new ArrayList<>();
        for (Teacher teacher : teachers1) {
            TeacherGradeDto teacherDto = new TeacherGradeDto(teacher);
            teacherGrades.add(teacherDto);
        }
    }

    public List<TeacherGradeDto> getTeacherGrades() {
        return teacherGrades;
    }

    public void setTeacherGrades(List<TeacherGradeDto> teacherGrades) {
        this.teacherGrades = teacherGrades;
    }
}
