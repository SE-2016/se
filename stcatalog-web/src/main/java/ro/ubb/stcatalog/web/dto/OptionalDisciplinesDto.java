package ro.ubb.stcatalog.web.dto;


import ro.ubb.stcatalog.core.model.OptionalDiscipline;

import java.util.ArrayList;
import java.util.List;


public class OptionalDisciplinesDto {
    List<OptionalDisciplineDto> optionalDisciplines;


    public OptionalDisciplinesDto() {
    }
    /*public OptionalDisciplinesDto(List<OptionalDiscipline> discipliness) {
        this.disciplines=discipliness;
    }*/
    public OptionalDisciplinesDto(List<OptionalDiscipline> optionalDisciplines) {
        this.optionalDisciplines = new ArrayList<>();
        for (OptionalDiscipline d : optionalDisciplines) {
            OptionalDisciplineDto temp = new OptionalDisciplineDto(d);
            this.optionalDisciplines.add(temp);
        }
    }

    public List<OptionalDisciplineDto> getOptionalDisciplines() {
        return optionalDisciplines;
    }

    public void setOptionalDisciplines(List<OptionalDisciplineDto> optionalDisciplines) {
        this.optionalDisciplines = optionalDisciplines;
    }
}
