package ro.ubb.stcatalog.web.dto;


public class OCDto {
    private OptionalDisciplineDto optionalDisciplineDto;

    public OCDto() {
    }

    public OCDto(OptionalDisciplineDto optionalDisciplineDto) {
        this.optionalDisciplineDto = optionalDisciplineDto;
    }

    public OptionalDisciplineDto getOptionalDisciplineDto() {
        return optionalDisciplineDto;
    }

    public void setOptionalDisciplineDto(OptionalDisciplineDto optionalDisciplineDto) {
        this.optionalDisciplineDto = optionalDisciplineDto;
    }
}
