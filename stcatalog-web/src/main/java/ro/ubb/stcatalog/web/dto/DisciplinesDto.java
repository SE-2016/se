package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.Discipline;

import java.util.ArrayList;
import java.util.List;


public class DisciplinesDto {
    List<DisciplineDto> disciplines;


    public DisciplinesDto() {
    }

    public DisciplinesDto(List<Discipline> disciplines) {
        this.disciplines = new ArrayList<>();
        for (Discipline d : disciplines) {
            DisciplineDto temp = new DisciplineDto(d);
            this.disciplines.add(temp);
        }
    }

    public List<DisciplineDto> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<DisciplineDto> disciplines) {
        this.disciplines = disciplines;
    }
}
