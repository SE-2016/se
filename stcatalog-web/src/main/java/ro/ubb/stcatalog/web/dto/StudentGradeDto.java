package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.person.Student;


public class StudentGradeDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String serialNumber;
    private String cnp;
    private int group;
    private float grade;

    public StudentGradeDto() {
    }

    public StudentGradeDto(String firstName, String lastName, String serialNumber, String cnp, int group, int grade) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.serialNumber = serialNumber;
        this.cnp = cnp;
        this.group = group;
        this.grade = grade;
    }

    public StudentGradeDto(Student student) {
        this.id = student.getId();
        this.firstName = student.getFirstName();
        this.lastName = student.getLastName();
        this.serialNumber = student.getSerialNumber();
        this.cnp = student.getCnp();
        this.group = student.getGroup().getNumber();
        this.grade = student.getGrade();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }
}
