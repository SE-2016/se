package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.OptionalDiscipline;


public class OptionalDisciplineDto {
    private Long id;
    private String disciplinegroup;
    private int priority;
    private String Name;
    private String Cod;
    private Long teacher_id;
    private String teacher;
    private String department;
    private int semester;
    public OptionalDisciplineDto() {
    }
    public OptionalDisciplineDto(OptionalDisciplineDto optionalDisciplineDto){
        this.id=optionalDisciplineDto.getId();
        this.disciplinegroup = optionalDisciplineDto.getDisciplinegroup();
        this.priority = optionalDisciplineDto.getPriority();
        this.Name = optionalDisciplineDto.getName();
        this.Cod = optionalDisciplineDto.getCod();
        this.teacher = optionalDisciplineDto.getTeacher();
        this.department = optionalDisciplineDto.getDepartment();
        this.semester=optionalDisciplineDto.getSemester();
        this.teacher_id=optionalDisciplineDto.getTeacher_id();
    }
    public OptionalDisciplineDto(OptionalDiscipline oc) {
        this.id=oc.getId();
        this.disciplinegroup = oc.getDisciplineGroup();
        this.priority = oc.getPriority();
        this.Name = oc.getName();
        this.Cod = oc.getCode();
        this.teacher = oc.getTeacher().getFirstName()+" "+oc.getTeacher().getLastName();
        this.department = oc.getDepartment().getName();
        this.semester=oc.getSemester().getNumber();
        this.teacher_id=oc.getTeacher().getId();
    }

    public String getDisciplinegroup() {
        return disciplinegroup;
    }

    public void setDisciplinegroup(String disciplinegroup) {
        this.disciplinegroup = disciplinegroup;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCod() {
        return Cod;
    }

    public void setCod(String cod) {
        Cod = cod;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public Long getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(Long teacher_id) {
        this.teacher_id = teacher_id;
    }

    @Override
    public String toString() {
        return "OptionalDisciplineDto{" +
                "id=" + id +
                ", disciplinegroup='" + disciplinegroup + '\'' +
                ", priority=" + priority +
                ", Name='" + Name + '\'' +
                ", Cod='" + Cod + '\'' +
                ", teacher='" + teacher + '\'' +
                ", department='" + department + '\'' +
                ", semester=" + semester +
                '}';
    }
}
