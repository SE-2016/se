package ro.ubb.stcatalog.web.dto;

import java.io.Serializable;

public class OnAddStudentDto implements Serializable {

    private String firstName;
    private String lastName;
    private String serialNumber;
    private String specialization;
    private String studyLevel;
    private String studyLine;
    private int semester;
    private int group;
    private String username;
    private String password;

    public OnAddStudentDto() {
    }

    public OnAddStudentDto(String firstName, String lastName, String serialNumber, String specialization, String studyLevel, String studyLine, int semester, int group, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.serialNumber = serialNumber;
        this.specialization = specialization;
        this.studyLevel = studyLevel;
        this.studyLine = studyLine;
        this.semester = semester;
        this.group = group;
        this.username = username;
        this.password = password;
    }

    public OnAddStudentDto(OnAddStudentDto s) {
        this.firstName = s.getFirstName();
        this.lastName = s.getFirstName();
        this.serialNumber = s.getSerialNumber();
        this.specialization = s.getSpecialization();
        this.studyLevel = s.getStudyLevel();
        this.studyLine = s.getStudyLine();
        this.semester = s.getSemester();
        this.group = s.getGroup();
        this.username = s.getUsername();
        this.password = s.getPassword();
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getStudyLevel() {
        return studyLevel;
    }

    public void setStudyLevel(String studyLevel) {
        this.studyLevel = studyLevel;
    }

    public String getStudyLine() {
        return studyLine;
    }

    public void setStudyLine(String studyLine) {
        this.studyLine = studyLine;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Override
    public String toString() {
        return "OnAddStudentDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", specialization='" + specialization + '\'' +
                ", studyLevel='" + studyLevel + '\'' +
                ", studyLine='" + studyLine + '\'' +
                ", semester=" + semester +
                ", group=" + group +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    /*
    public OnAddStudentDto(Student student) {
        this.student = student;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
    public String getFirstName() {
        return  student.getFirstName();
    }

    public void setFirstName(String firstName) {
        this.student.setFirstName( firstName);
    }

    public String getLastName() {
        return student.getLastName();
    }

    public void setLastName(String lastName) {
        this.student.setLastName( lastName);
    }

    public String getSerialNumber() {
        return student.getSerialNumber();
    }

    public void setSerialNumber(String serialNumber) {
        this.student.setSerialNumber( serialNumber);
    }

    public String getSpecialization() {
        return student.getSpecialization();
    }

    public void setSpecialization(String specialization) {
        this.student.setSpecialization( specialization);
    }

    public String getStudyLevel() {
        return student.getStudyLevel();
    }

    public void setStudyLevel(String studyLevel) {
        this.student.setStudyLevel( studyLevel);
    }

    public String getStudyLine() {
        return student.getStudyLine();
    }

    public void setStudyLine(String studyLine) {
        this.student.setStudyLine( studyLine);
    }

    public int getSemester() {
        return student.getSemester();
    }

    public void setSemester(int semester) {
        this.student.setSemester( semester);
    }

    public int getGroup() {
        return student.getGroup();
    }

    public void setGroup(int group) {
        this.setGroup( group);
    }

    public String getUsername() {
        return getUsername();
    }

    public void setUsername(String username) {
        this.student.setUsername( username);
    }

    public String getPassword() {
        return student.getPassword();
    }

    public void setPassword(String password) {
        this.student.setPassword( password);
    }

    */

}
