package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.person.Teacher;

import java.util.ArrayList;
import java.util.List;


public class TeachersDto {
    private List<TeacherDto> teachers;

    public TeachersDto() {
    }

    public TeachersDto(List<Teacher> teachers1) {
        teachers = new ArrayList<>();
        for (Teacher teacher : teachers1) {
            TeacherDto teacherDto = new TeacherDto(teacher);
            teachers.add(teacherDto);
        }
    }

    public List<TeacherDto> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<TeacherDto> teachers) {
        this.teachers = teachers;
    }


}
