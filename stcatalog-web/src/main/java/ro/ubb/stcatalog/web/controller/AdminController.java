package ro.ubb.stcatalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.model.person.Teacher;
import ro.ubb.stcatalog.core.service.Interfaces.AdminService;
import ro.ubb.stcatalog.core.service.Interfaces.StudentService;
import ro.ubb.stcatalog.web.dto.StudentDto;
import ro.ubb.stcatalog.web.dto.StudentGradeList;
import ro.ubb.stcatalog.web.dto.StudentsDto;
import ro.ubb.stcatalog.web.dto.TeacherDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@RestController
public class AdminController {

    private static final Logger log = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private AdminService adminService;

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/onAddStudents", method = RequestMethod.POST, produces = "application/vnd.api+json")
    public Map<String, StudentDto> createOnAddStudent(final HttpServletRequest request) throws IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String cnp = request.getParameter("cnp");
        String serialNumber = request.getParameter("serialNumber");
        String specialization = request.getParameter("specialization");
        String studyLevel = request.getParameter("studyLevel");
        String studyLine = request.getParameter("studyLine");
        int semester = Integer.parseInt(request.getParameter("semester"));
        int group = Integer.parseInt(request.getParameter("group"));
        String username = request.getParameter("username");
        String password = request.getParameter("password");


        Student student = adminService.createStudent(firstName, lastName,cnp, serialNumber, specialization, studyLevel, studyLine, semester, group, username, password);

        Map<String, StudentDto> studentDtoMap = new HashMap<>();
        studentDtoMap.put("student", new StudentDto(student));

        log.trace("createStudent: studentDtoMap={}", studentDtoMap);

        return studentDtoMap;
    }
    @RequestMapping(value = "/setToF", method = RequestMethod.POST)
    public void setTOF(final HttpServletRequest request,final HttpServletResponse response) throws IOException {
        String TOF = request.getParameter("TOF");
        adminService.setToF(TOF);
    }
    @RequestMapping(value = "/onAddTeachers", method = RequestMethod.POST, produces = "application/vnd.api+json")
    public Map<String, TeacherDto> createOnAddTeacher(final HttpServletRequest request) throws IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String cnp = request.getParameter("cnp");
        String rank = request.getParameter("rank");
        String depart = request.getParameter("department");
        String username = request.getParameter("username");
        String password = request.getParameter("password");


        Teacher teacher = adminService.createTeacher(firstName, lastName, cnp, rank, depart, username, password);

        Map<String, TeacherDto> teacherDtoMap = new HashMap<>();
        teacherDtoMap.put("teacher", new TeacherDto(teacher));

        log.trace("createStudent: studentDtoMap={}", teacherDtoMap);//PENTRU DEBUG

        return teacherDtoMap;
    }

    @RequestMapping(value = "/onAddCOD", method = RequestMethod.POST, produces = "application/vnd.api+json")
    public Map<String, TeacherDto> createOnAddCOD(final HttpServletRequest request) throws IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String cnp = request.getParameter("cnp");
        String rank = request.getParameter("rank");
        String depart = request.getParameter("department");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        Map<String, TeacherDto> CODDtoMap = new HashMap<>();


        Teacher teacher = adminService.createCOD(firstName, lastName, cnp, rank, depart, username, password);
        if (teacher != null) {
            CODDtoMap.put("teacher", new TeacherDto(teacher));


            log.trace("createStudent: studentDtoMap={}", CODDtoMap);//PENTRU DEBUG

            return CODDtoMap;
        }
        return null;
    }

    @RequestMapping(value = "/studentGrades", params = {"spec", "studyLevel", "studyLine", "promotion"}, method = RequestMethod.GET, produces = "application/vnd.api+json")
    public
    @ResponseBody
    StudentGradeList getStudentsByPromotion(@RequestParam(value = "spec") final String spec, @RequestParam(value = "studyLevel") final String studyLevel,
                                            @RequestParam(value = "studyLine") final String studyLine, @RequestParam(value = "promotion") final String promotion) {
        log.trace("getStudentGrades");
        List<Student> studentList=adminService.getStudentsByPromotion(spec,studyLevel,studyLine,promotion);
        Collections.sort(studentList, new Comparator() {

            public int compare(Object o1, Object o2) {

                float x1 = ((Student) o1).getGrade();
                float x2 = ((Student) o2).getGrade();
                int sComp = Float.compare(x2, x1);

                if (sComp != 0) {
                    return sComp;
                } else {
                    String x11 = ((Student) o1).getFirstName();
                    String x22 = ((Student) o2).getFirstName();
                    return x11.compareTo(x22);
                }
            }
        });

        log.trace("getStudentsByPromotion: studentGrades={}", studentList);
        return new StudentGradeList(studentList);
    }

    @RequestMapping(value = "/students",params = {"spec","studyLevel","studyLine","promotion","group"}, method = RequestMethod.GET, produces = "application/vnd.api+json")
    public
    @ResponseBody
    StudentsDto getStudentsByGroup(@RequestParam(value = "spec") final String spec ,@RequestParam(value = "studyLevel") final String studyLevel,
                                   @RequestParam(value = "studyLine") final String studyLine,@RequestParam(value = "promotion") final String promotion,
                                   @RequestParam(value = "group") final int group) {
        log.trace("getStudents");

        List<Student> students = studentService.findAll();
        List<Student> filteredList = students.stream().filter(s-> s.getGroup().getPromotion().equals(promotion) )
                .filter(s->s.getGroup().getGroupNumber() == group)
                .filter(s->s.getGroup().getSemester().getStudyLine().getType().equals(studyLine))
                .filter(s->s.getGroup().getSemester().getStudyLine().getStudyLevel().getType().equals(studyLevel))
                .filter(s->s.getGroup().getSemester().getStudyLine().getStudyLevel().getSpecialization().getName().equals(spec))
                .collect(Collectors.toList());
        log.trace("getStudentsByPromotion: students={}", filteredList);
        return new StudentsDto(filteredList);
    }
}
