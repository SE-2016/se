package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.person.Student;

import java.util.ArrayList;
import java.util.List;


public class StudentGradeList {
    private List<StudentGradeDto> studentGrades;

    public StudentGradeList() {
    }

    public StudentGradeList(List<Student> students1) {
        studentGrades = new ArrayList<>();
        for (Student s : students1) {
            StudentGradeDto studentGradeDto = new StudentGradeDto(s);
            studentGrades.add(studentGradeDto);
        }
    }

    public List<StudentGradeDto> getStudentGrades() {
        return studentGrades;
    }

    public void setStudentGrades(List<StudentGradeDto> students) {
        this.studentGrades = students;
    }
}
