package ro.ubb.stcatalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.stcatalog.core.model.*;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.repository.Contract_OptDisciplineRepository;
import ro.ubb.stcatalog.core.repository.StudentRepository;
import ro.ubb.stcatalog.core.repository.optionalDisciplineRepository;
import ro.ubb.stcatalog.core.service.Interfaces.StudentService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class StudentServiceImpl implements StudentService {

    private static final Logger log = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Qualifier("studentRepository")
    @Autowired
    private StudentRepository studentRepository;

    @Qualifier("optionalDisciplineRepository")
    @Autowired
    private optionalDisciplineRepository optionalDisciplineRepository;

    @Qualifier("contract_OptDisciplineRepository")
    @Autowired
    private Contract_OptDisciplineRepository contract_OptDisciplineRepository;

    @Override
    @Transactional
    public List<Student> findAll() {
        log.trace("findAll");

        List<Student> students = studentRepository.findAll();

        log.trace("findAll: students={}", students);

        return students;
    }

    @Override
    @Transactional
    public Student store(Student s) {
        log.trace("store");

        Student students = studentRepository.save(s);

        log.trace("student: store={}", students);

        return students;
    }
    @Override
    @Transactional
    public Optional<Student> LogIn(String user, String pass){
        //Optional<Student> s=studentRepository.findByUserNameAndUserPassword(user,pass);
        return null;
    }
    @Override
    @Transactional
    public List<Discipline> findAllDisciplines(Long studentId) {

        List<Discipline> disciplines = studentRepository.findDisciplinesByStudentId(studentId);
        return disciplines;
    }

    @Override
    @Transactional
    public List<Discipline> findAllDisciplinesByYear(Long studentId, Long year) {

        /*Student student = studentRepository.findOne(studentId);
        List<Discipline> disciplines=new ArrayList<>();
        List<StudyContract> studyContract = student.getStudyContracts();
        for(StudyContract studyContract1:studyContract)
            disciplines.addAll(studyContract1.getDisciplines().stream().collect(Collectors.toList()));*/
        List<Discipline> disciplines=studentRepository.findDisciplinesByStudentId(studentId);
        List<Discipline> f1nal = new ArrayList<>();
        if (year == 1)
            f1nal = disciplines.stream()
                    .filter(p -> p.getSemester().getNumber() < 3).collect(Collectors.toList());
        if (year == 2)
            f1nal = disciplines.stream()
                    .filter(p -> p.getSemester().getNumber() > 2 && p.getSemester().getNumber() < 5).collect(Collectors.toList());
        if (year == 3)
            f1nal = disciplines.stream()
                    .filter(p -> p.getSemester().getNumber() > 4 && p.getSemester().getNumber() < 7).collect(Collectors.toList());
        if (year == 0)
            f1nal = disciplines;
        return f1nal;
    }

    @Transactional
    @Override
    public List<Discipline> findAllDisciplinesFailed(Long studentId) {
        List<Discipline> disciplines = studentRepository.findFailedDisciplines(studentId);
        return disciplines;
    }

    @Override
    public List<OptionalDiscipline> findAOCByStudentAndYear(Long id, Long year) {
        Student student = studentRepository.findOne(id);
        StudyLine studyLine = student.getGroup().getSemester().getStudyLine();
        List<OptionalDiscipline> optionalDisciplines = new ArrayList<>();
        for (Semester s : studyLine.getSemesters()) {
            if (year == 1)
                if (s.getNumber() < 3) {
                    List<OptionalDiscipline> l1st = setPriority(s.getOptionalDisciplines(), id);
                    optionalDisciplines.addAll(l1st);
                }
            if (year == 2)
                if (s.getNumber() > 2 && s.getNumber() < 5) {
                    List<OptionalDiscipline> l1st = setPriority(s.getOptionalDisciplines(), id);
                    optionalDisciplines.addAll(l1st);
                }
            if (year == 3)
                if (s.getNumber() > 4) {
                    List<OptionalDiscipline> l1st = setPriority(s.getOptionalDisciplines(), id);
                    optionalDisciplines.addAll(l1st);
                }
        }
        return optionalDisciplines;
    }

    @Override
    @Transactional
    public Contract_OptDiscipline updateOC(long id, int id_student, int priority, int year) {

        OptionalDiscipline optionalDiscipline= optionalDisciplineRepository.findOne(id);
        Student student = studentRepository.findOne(Long.valueOf(id_student));
        List<StudyContract> studyContracts = student.getStudyContracts();
        Contract_OptDiscipline contract_optDiscipline = new Contract_OptDiscipline();
        for (StudyContract studyContract : studyContracts)
            if (studyContract.getYear() == year) {
                if (studentRepository.findCt_OC(studyContract.getId(), optionalDiscipline.getId()).isPresent()) {
                    contract_optDiscipline = studentRepository.findCt_OC(studyContract.getId(), optionalDiscipline.getId()).get();
                    contract_optDiscipline.setPriority(priority);
                    contract_OptDisciplineRepository.saveAndFlush(contract_optDiscipline);
                } else {

                    contract_optDiscipline.setPriority(priority);
                    contract_optDiscipline.setOptionalDiscipline(optionalDiscipline);
                    contract_optDiscipline.setStudyContract(studyContract);
                    contract_OptDisciplineRepository.save(contract_optDiscipline);
                }
                break;
            }

        return contract_optDiscipline;
    }

    public List<OptionalDiscipline> setPriority(List<OptionalDiscipline> optionalDisciplines, Long id) {
        List<OptionalDiscipline> l1st = optionalDisciplines;
        for (OptionalDiscipline optionalDiscipline : l1st) {
            List<Contract_OptDiscipline> contract_optDisciplines = optionalDiscipline.getContract_optDisciplines();
            contract_optDisciplines.stream().filter(opt -> opt.getStudyContract().getStudent().getId() == id).forEach(opt -> optionalDiscipline.setPriority(opt.getPriority()));
        }
        return l1st;
    }
}
