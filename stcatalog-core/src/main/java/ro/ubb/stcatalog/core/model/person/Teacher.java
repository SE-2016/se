package ro.ubb.stcatalog.core.model.person;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ro.ubb.stcatalog.core.model.Department;
import ro.ubb.stcatalog.core.model.OptionalDiscipline;
import ro.ubb.stcatalog.core.model.TeacherIn;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
@Table(name="Teacher")
public class Teacher extends Person {
    @Transient
    private float grade = 0;


    @ManyToOne
    @JoinColumn(name = "department_id")
    @JsonBackReference
    private Department department;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "teacher")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    private List<OptionalDiscipline> optionalDisciplines;

    @Column(name="rank")
    private String rank;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "teacher")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    private List<TeacherIn> teacherInList;

    public Teacher(){
        optionalDisciplines = new ArrayList<OptionalDiscipline>();
        teacherInList = new ArrayList<TeacherIn>();
    }
    public Teacher(Long id,String firsName,String lastName, String cnp,String rank){
        super(id,firsName,lastName,cnp);
        this.rank=rank;
        optionalDisciplines = new ArrayList<OptionalDiscipline>();
    }
    public Teacher(String firstName, String lastName, String cnp, String rank) {
        super(firstName, lastName, cnp);
        this.rank = rank;
    }
    public List<TeacherIn> getTeacherInList() {
        return teacherInList;
    }

    public void setTeacherInList(List<TeacherIn> teacherInList) {
        this.teacherInList = teacherInList;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }


    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }

    public List<OptionalDiscipline> getOptionalDisciplines() {
        return optionalDisciplines;
    }

    public void setOptionalDisciplines(List<OptionalDiscipline> optionalDisciplines) {
        this.optionalDisciplines = optionalDisciplines;
    }
}
