package ro.ubb.stcatalog.core.model;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Auxiliar")
public class Auxiliar extends BaseEntity<Long>{
    @Column(name = "time_of_year")
    private String time_of_year;

    public String getTime_of_year() {
        return time_of_year;
    }

    public void setTime_of_year(String time_of_year) {
        this.time_of_year = time_of_year;
    }
}
