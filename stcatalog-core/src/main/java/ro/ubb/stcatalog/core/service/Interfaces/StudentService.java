package ro.ubb.stcatalog.core.service.Interfaces;

import ro.ubb.stcatalog.core.model.Contract_OptDiscipline;
import ro.ubb.stcatalog.core.model.Discipline;
import ro.ubb.stcatalog.core.model.OptionalDiscipline;
import ro.ubb.stcatalog.core.model.person.Student;

import java.util.List;
import java.util.Optional;


public interface StudentService {
    List<Student> findAll();
    Student store(Student s);
    Optional<Student> LogIn(String user, String pass);
    List<Discipline> findAllDisciplines(Long studentId);

    Contract_OptDiscipline updateOC(long id, int id_student, int priority, int year);
    List<Discipline> findAllDisciplinesByYear(Long studentId, Long year);

    List<Discipline> findAllDisciplinesFailed(Long studentId);

    List<OptionalDiscipline> findAOCByStudentAndYear(Long id, Long year);
}
