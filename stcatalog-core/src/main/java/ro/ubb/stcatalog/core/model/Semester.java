package ro.ubb.stcatalog.core.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Semester")
public class Semester extends BaseEntity<Long>{


    @Column(name="number")
    private int number;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "semester")
    @Fetch(FetchMode.SUBSELECT)
    private List<Group> groups;

    @ManyToOne
    @JoinColumn(name = "studyLine_id")
    private StudyLine studyLine;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "semester")
    @Fetch(FetchMode.SUBSELECT)
    private List<Discipline> disciplines;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "semester")
    @Fetch(FetchMode.SUBSELECT)
    private List<OptionalDiscipline> optionalDisciplines;

    public Semester(){
    }
    public Semester(Long id, int number){
        super(id);
        this.number=number;
        groups = new ArrayList<>();
        disciplines = new ArrayList<>();
    }

    public Group getGroupByNumber(int number) {
        for (Group group : this.groups) {
            if (group.getNumber() == number)
                return group;
        }
        return this.groups.get(0);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }


    public StudyLine getStudyLine() {
        return studyLine;
    }

    public void setStudyLine(StudyLine studyLine) {
        this.studyLine = studyLine;
    }


    public List<Discipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<Discipline> disciplines) {
        this.disciplines = disciplines;
    }

    public List<OptionalDiscipline> getOptionalDisciplines() {
        return optionalDisciplines;
    }

    public void setOptionalDisciplines(List<OptionalDiscipline> optionalDisciplines) {
        this.optionalDisciplines = optionalDisciplines;
    }
}