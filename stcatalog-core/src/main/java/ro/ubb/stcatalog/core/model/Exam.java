package ro.ubb.stcatalog.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ro.ubb.stcatalog.core.model.person.Student;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="Exam")

public class Exam extends BaseEntity<Long> {
    //one student many exam
    //one discipline one exam
    @Column(name = "examinationData")
    private Date examinationDate;
    @Column(name = "reExaminationData")
    private Date reExaminationDate;
    @Column(name = "examinationGrade")
    private float examinationGrade;
    @Column(name = "reExaminationGrade")
    private float reExaminationGrade;
    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "discipline_id")
    private Discipline discipline;

    public Exam(Long id){
        super(id);
    }

    public Exam(){

    }

    public Date getExaminationDate() {
        return examinationDate;
    }

    public void setExaminationDate(Date examinationDate) {
        this.examinationDate = examinationDate;
    }

    public Date getReExaminationDate() {
        return reExaminationDate;
    }

    public void setReExaminationDate(Date reExaminationDate) {
        this.reExaminationDate = reExaminationDate;
    }

    public float getExaminationGrade() {
        return examinationGrade;
    }

    public void setExaminationGrade(float examinationGrade) {
        this.examinationGrade = examinationGrade;
    }

    public float getReExaminationGrade() {
        return reExaminationGrade;
    }

    public void setReExaminationGrade(float reExaminationGrade) {
        this.reExaminationGrade = reExaminationGrade;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Discipline getDiscipline() {
        return discipline;
    }

    public void setDiscipline(Discipline discipline) {
        this.discipline = discipline;
    }
}
