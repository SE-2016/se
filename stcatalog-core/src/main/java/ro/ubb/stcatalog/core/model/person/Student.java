package ro.ubb.stcatalog.core.model.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ro.ubb.stcatalog.core.model.Discipline;
import ro.ubb.stcatalog.core.model.Exam;
import ro.ubb.stcatalog.core.model.Group;
import ro.ubb.stcatalog.core.model.StudyContract;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Student")
public class Student extends Person {
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "student")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    List<Exam> exams;
    @Transient
    private float grade = 0;
    @Column(name = "serialNumber")
    private String serialNumber;
    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "student")
    @JsonIgnore
    private List<StudyContract> studyContracts;

    @ManyToMany(cascade = {CascadeType.ALL})
    //@Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "FailedDisciplines",
            joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = {@JoinColumn(name = "discipline_id")})
    private List<Discipline> disciplines;

    public Student(){

    }
    public Student(Long id, String firstName,String lastName,String cnp){
        super(id,firstName,lastName,cnp);
        exams = new ArrayList<Exam>();
    }
    public Student(Long id, String firstName,String lastName,String cnp,String serialNumber){
        super(id,firstName,lastName,cnp);
        this.serialNumber = serialNumber;
        exams = new ArrayList<Exam>();
    }
    public Student(String firstName,String lastName,String cnp,String serialNumber){
        super(firstName,lastName,cnp);
        this.serialNumber = serialNumber;
        exams = new ArrayList<Exam>();
    }

    public Student(String firstName, String lastName, String cnp, String serialNumber, Group group) {
        super(firstName, lastName, cnp);
        this.serialNumber = serialNumber;
        this.group = group;
        studyContracts=new ArrayList<>();
        exams = new ArrayList<Exam>();
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }



    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<Exam> getExams() {
        return exams;
    }

    public void setExams(List<Exam> exams) {
        this.exams = exams;
    }

    public List<StudyContract> getStudyContracts() {
        return studyContracts;
    }

    public void setStudyContracts(List<StudyContract> studyContracts) {
        this.studyContracts = studyContracts;
    }

    public List<Discipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<Discipline> disciplines) {
        this.disciplines = disciplines;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }
}
