package ro.ubb.stcatalog.core.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ro.ubb.stcatalog.core.model.person.Teacher;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="OptionalDiscipline")
public class OptionalDiscipline extends BaseEntity<Long> {

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "optionalDiscipline")
    @Fetch(value = FetchMode.SUBSELECT)
    List<Contract_OptDiscipline> contract_optDisciplines;
    @Column(name = "name")
    private String name;
    @Column(name = "cod")
    private String code;
    @ManyToOne
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;
    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "semester_id")
    private Semester semester;

    @Column(name = "disciplineGroup")
    private String disciplineGroup;

    @Transient
    private int priority = 0;

    public OptionalDiscipline(Long id, String code, String name){
        super(id);
        this.code = code;
        this.name = name;
    }
    public OptionalDiscipline(){

    }

    public String getDisciplineGroup() {
        return disciplineGroup;
    }

    public void setDisciplineGroup(String disciplineGroup) {
        this.disciplineGroup = disciplineGroup;
    }



    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public List<Contract_OptDiscipline> getContract_optDisciplines() {
        return contract_optDisciplines;
    }

    public void setContract_optDisciplines(List<Contract_OptDiscipline> contract_optDisciplines) {
        this.contract_optDisciplines = contract_optDisciplines;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}