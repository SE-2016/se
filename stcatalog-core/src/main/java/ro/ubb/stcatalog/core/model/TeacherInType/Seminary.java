    package ro.ubb.stcatalog.core.model.TeacherInType;


    import com.fasterxml.jackson.annotation.JsonManagedReference;
    import ro.ubb.stcatalog.core.model.Discipline;
    import ro.ubb.stcatalog.core.model.TeacherIn;

    import javax.persistence.Entity;
    import javax.persistence.OneToMany;
    import javax.persistence.Table;
    import java.util.List;

    @Entity
    @Table(name="Seminary")
    public class Seminary extends TeacherIn {


        @OneToMany(mappedBy = "seminary")
        @JsonManagedReference
        private List<Discipline> disciplines;


        public Seminary() {

        }

        public List<Discipline> getDisciplines() {
            return disciplines;
        }

        public void setDisciplines(List<Discipline> disciplines) {
            this.disciplines = disciplines;
        }



    }
