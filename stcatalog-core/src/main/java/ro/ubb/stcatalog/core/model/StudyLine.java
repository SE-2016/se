package ro.ubb.stcatalog.core.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="StudyLine")

public class StudyLine extends BaseEntity<Long>{

    @Column(name="type")
    private String type;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "studyLine")
    @Fetch(FetchMode.SUBSELECT)
    private List<Semester> semesters;

    @ManyToOne
    @JoinColumn(name = "studyLevel_id")
    private StudyLevel studyLevel;

    public StudyLine(){
    }

    public StudyLine(Long id, String type){
        super(id);
        this.type=type;
    }

    public Semester getSemesterByNumber(int number) {
        for (Semester semester : this.semesters) {
            if (semester.getNumber() == number)
                return semester;
        }
        return this.semesters.get(0);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Semester> getSemesters() {
        return semesters;
    }

    public void setSemesters(List<Semester> semesters) {
        this.semesters = semesters;
    }

    public StudyLevel getStudyLevel() {
        return studyLevel;
    }

    public void setStudyLevel(StudyLevel studyLevel) {
        this.studyLevel = studyLevel;
    }


}
