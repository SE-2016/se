package ro.ubb.stcatalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.stcatalog.core.model.Discipline;
import ro.ubb.stcatalog.core.repository.DisciplineRepository;
import ro.ubb.stcatalog.core.service.Interfaces.DisciplineService;

import java.util.List;


@Service
public class DisciplineServiceImpl implements DisciplineService {

    private static final Logger log = LoggerFactory.getLogger(DisciplineServiceImpl.class);

    @Qualifier("disciplineRepository")
    @Autowired
    private DisciplineRepository disciplineRepository;

    @Override
    @Transactional
    public List<Discipline> findDisciplineOfStudent(Long studentId) {
        log.trace("findAllByStydent");

        List<Discipline> disciplines= disciplineRepository.findAll();

        log.trace("findAll: Disciplines={}", disciplines);

        return disciplines;
    }

}
