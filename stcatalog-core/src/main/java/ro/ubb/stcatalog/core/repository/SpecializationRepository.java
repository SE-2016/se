package ro.ubb.stcatalog.core.repository;

import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.Specialization;

@Repository
public interface SpecializationRepository extends CatalogRepository<Specialization, Long>{
}
