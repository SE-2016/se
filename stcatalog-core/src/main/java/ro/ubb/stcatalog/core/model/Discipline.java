package ro.ubb.stcatalog.core.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ro.ubb.stcatalog.core.model.TeacherInType.Laboratory;
import ro.ubb.stcatalog.core.model.TeacherInType.Lecture;
import ro.ubb.stcatalog.core.model.TeacherInType.Seminary;
import ro.ubb.stcatalog.core.model.person.Student;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Discipline")
public class Discipline extends BaseEntity<Long> {

    @ManyToMany(mappedBy = "disciplines")
    List<StudyContract> studyContracts;

    @ManyToMany(mappedBy = "disciplines")
    List<Student> students;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "discipline")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    List<Exam> exams;
    @Column(name = "code")
    private String code;

    @Column(name = "credits")
    private int credits;

    @Column(name = "name")
    private String name;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "semester_id")
    private Semester semester;
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "lecture_id")
    private Lecture lecture;
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "seminary_id")
    private Seminary seminary;
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "laboratoryid")
    private Laboratory laboratory;

    public Discipline() {

    }

    public Discipline(Long id, String code, String name) {
            super(id);
            this.code = code;
            this.name = name;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public List<Exam> getExams() {
        return exams;
    }

    public void setExams(List<Exam> exams) {
        this.exams = exams;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



        public Lecture getLecture() {
            return lecture;
        }

        public void setLecture(Lecture lecture) {
            this.lecture = lecture;
        }

        public Seminary getSeminary() {
            return seminary;
        }

        public void setSeminary(Seminary seminary) {
            this.seminary = seminary;
        }

        public Laboratory getLaboratory() {
            return laboratory;
        }

        public void setLaboratory(Laboratory laboratory) {
            this.laboratory = laboratory;
        }

    public List<StudyContract> getStudyContracts() {
        return studyContracts;
    }

    public void setStudyContracts(List<StudyContract> studyContracts) {
        this.studyContracts = studyContracts;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }
}
