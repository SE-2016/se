package ro.ubb.stcatalog.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.Discipline;
import ro.ubb.stcatalog.core.model.person.Teacher;

import java.util.List;

/**
 * Created by radu.
 */

@Repository
public interface TeacherRepository extends CatalogRepository<Teacher, Long> {
    @Query("select s from Teacher s , ChiefOfDepartment c where s.id=:id and c.id=:id ")
    Teacher findCod(@Param("id") Long id);

    @Query("select s from Teacher s")
    List<Teacher> findAll();

}
