package ro.ubb.stcatalog.core.repository;

import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.Contract_OptDiscipline;

@Repository
public interface Contract_OptDisciplineRepository extends CatalogRepository<Contract_OptDiscipline, Long> {
}
