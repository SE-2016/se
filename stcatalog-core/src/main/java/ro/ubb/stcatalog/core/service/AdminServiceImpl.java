package ro.ubb.stcatalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.stcatalog.core.model.*;
import ro.ubb.stcatalog.core.model.person.ChiefOfDepartment;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.model.person.Teacher;
import ro.ubb.stcatalog.core.model.person.Users;
import ro.ubb.stcatalog.core.repository.*;
import ro.ubb.stcatalog.core.service.Interfaces.AdminService;

import java.util.List;
import java.util.Optional;

@Service
public class AdminServiceImpl implements AdminService {

    private static final Logger log = LoggerFactory.getLogger(AdminServiceImpl.class);

    @Qualifier("adminRepository")
    @Autowired
    private AdminRepository adminRepository;

    @Qualifier("studentRepository")
    @Autowired
    private StudentRepository studentRepository;

    @Qualifier("logInRepository")
    @Autowired
    private LogInRepository logInRepository;

    @Qualifier("teacherRepository")
    @Autowired
    private TeacherRepository teacherRepository;

    @Qualifier("CODRepository")
    @Autowired
    private CODRepository CODRepository;

    @Qualifier("specializationRepository")
    @Autowired
    private SpecializationRepository specializationRepository;

    @Override
    @Transactional
    public Student createStudent(String firstName, String lastName,String cnp, String serialNumber,
                                 String specialization, String studyLevel, String studyLine, int semester,
                                 int group, String username, String password) {

        Student student = new Student(firstName, lastName, cnp, serialNumber);

        //set the group
        Optional<Specialization> specialization1 = adminRepository.findSpecializationByName(specialization);
        Specialization specialization2 = specialization1.get();
        StudyLevel studyLevel1 = specialization2.getStudyLevelByType(studyLevel);
        StudyLine studyLine1 = studyLevel1.getStudyLineByType(studyLine);
        Semester semester1 = studyLine1.getSemesterByNumber(semester);
        Group group1 = semester1.getGroupByNumber(group);
        student.setGroup(group1);


        Student student1 = studentRepository.save(student);

        Users user1 = new Users();
        user1.setPassword(password);
        user1.setUserName(username);
        user1.setStudent(student1);
        user1.setRole("student");
        logInRepository.save(user1);
        log.trace("createStudent: student={}", student1);

        return student1;
    }
    @Override
    @Transactional
    public void setToF(String tof){
        Auxiliar auxiliar=logInRepository.getToF().get();
        auxiliar.setTime_of_year(tof);
    }

    @Override
    @Transactional
    public Teacher createTeacher(String firstName, String lastName, String cnp, String rank, String departName, String username, String password) {

        Teacher teacher = new Teacher(firstName, lastName, cnp, rank);

        //set the relations with other entities
        Optional<Department> department = adminRepository.findDepartmentNyName(departName);
        Department department1 = department.get(); //if the department does not exist it'll throws an error
        teacher.setDepartment(department1);
        Teacher teacher1 = teacherRepository.save(teacher);

        //set the teacher as a user
        Users user = new Users();
        user.setPassword(password);
        user.setUserName(username);
        user.setTeacher(teacher1);
        logInRepository.save(user);

        log.trace("createTeacher: teacher-{}", teacher1);

        return teacher1;
    }


    @Transactional
    @Override
    public ChiefOfDepartment createCOD(String firstName, String lastName, String cnp, String rank, String departName, String username, String password) {

        //set the relations with other entities
        List<ChiefOfDepartment> chiefOfDepartmentList=adminRepository.getAllDepartments();
        ChiefOfDepartment cod1 = ChiefOfDepartment.getInstance();
        for(Teacher chief:chiefOfDepartmentList) {
            String c = chief.getDepartment().getName();
            if (c.equals(departName))
                return null;
        }
        cod1.setFirstName(firstName);
        cod1.setLastName(lastName);
        cod1.setCnp(cnp);
        cod1.setRank(rank);
        cod1.setDepartment(adminRepository.findDepartmentNyName(departName).get());
        teacherRepository.save(cod1);
        CODRepository.saveAndFlush(cod1);
        Users user = new Users();
        user.setPassword(password);
        user.setUserName(username);
        user.setChiefOfDepartment(cod1);
        user.setRole("cod");
        logInRepository.save(user);

        //if the department does not exist it'll throws an error
        return cod1;
    }

    @Override
    public List<Student> getStudentsByPromotion(String spec, String studyLevel, String studyLine, String promotion) {
        List<Student> students=adminRepository.getStudentsByPromotion(spec,studyLevel,studyLine,promotion);
        for (Student student : students) {
            int grade = 0;
            float sum = 0;
            int i = 0;
            List<Exam> exams = student.getExams();
            for (Exam exam : exams) {
                if (exam.getDiscipline().getSemester().getNumber() == student.getGroup().getSemester().getNumber() - 1) {
                    float g1 = Math.max(exam.getExaminationGrade(), exam.getReExaminationGrade());
                    sum += g1;
                    i++;
                }
            }
            if (i == 0)
                student.setGrade(0);
            else
                student.setGrade((sum / i));
        }
        return students;
    }

}
