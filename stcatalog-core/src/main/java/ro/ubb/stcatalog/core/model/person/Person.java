package ro.ubb.stcatalog.core.model.person;


import ro.ubb.stcatalog.core.model.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
@Inheritance(strategy=InheritanceType.JOINED)
public class Person extends BaseEntity<Long> {

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "cnp")
    private String cnp;

    public Person(){}
    public Person(Long id, String firstName, String lastName, String cnp){
        super(id);
        this.firstName=firstName;
        this.lastName=lastName;
        this.cnp=cnp;
    }
    public Person(String firstName, String lastName, String cnp){
        this.firstName=firstName;
        this.lastName=lastName;
        this.cnp=cnp;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }


}
