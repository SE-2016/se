package ro.ubb.stcatalog.core.service.Interfaces;

import ro.ubb.stcatalog.core.model.person.ChiefOfDepartment;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.model.person.Teacher;

import java.util.List;

/**
 * Created by Ileni on 06/05/2016.
 */

public interface AdminService {
    Student createStudent(String firstName, String lastName,String cnp, String serialNumber, String specialization, String studyLevel, String studyLine, int semester, int group, String username, String password);
    void setToF(String tof);
    Teacher createTeacher(String firstName, String lastName, String cnp, String rank, String departName, String username, String password);

    ChiefOfDepartment createCOD(String firstName, String lastName, String cnp, String rank, String departName, String username, String password);
    List<Student> getStudentsByPromotion(String spec , String studyLevel, String studyLine, String promotion);
}
