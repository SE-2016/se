package ro.ubb.stcatalog.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.Auxiliar;
import ro.ubb.stcatalog.core.model.person.Users;

import java.util.Optional;

@Repository
public interface LogInRepository extends CatalogRepository<Users, Long> {
    @Query("select s from Users s where s.userName=:user and s.password=:password and s.role='student' ")
    Optional<Users> findStudentLogin(@Param("user") String username, @Param("password")String password);

    @Query("select s from Users s where s.userName=:user and s.password=:password and s.role='teacher'")
    Optional<Users> findTeacherLogin(@Param("user") String username, @Param("password")String userpassword);

    @Query("select s from Users s where s.userName=:user and s.password=:password and s.role='cod'")
    Optional<Users> findCodLogin(@Param("user") String username, @Param("password")String userpassword);

    @Query("select s from Users s where s.userName=:user and s.password=:password")
    Optional<Users> logIn(@Param("user") String username, @Param("password")String password);

    @Query("select s from Users s where s.userName=:user and s.password=:password and s.role='admin'")
    Optional<Users> findAdminLogin(String user, String pass);

    @Query("select s from Auxiliar s")
    Optional<Auxiliar> getToF();
}