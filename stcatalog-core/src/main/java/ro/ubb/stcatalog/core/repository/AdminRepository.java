package ro.ubb.stcatalog.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.Department;
import ro.ubb.stcatalog.core.model.Specialization;
import ro.ubb.stcatalog.core.model.person.Administer;
import ro.ubb.stcatalog.core.model.person.ChiefOfDepartment;
import ro.ubb.stcatalog.core.model.person.Student;

import java.util.List;
import java.util.Optional;



@Repository
public interface AdminRepository extends CatalogRepository<Administer, Long> {

    @Query("select s from Specialization s where s.name=:specialization")
    Optional<Specialization> findSpecializationByName(@Param("specialization") String specialization);

    @Query("select d from  ChiefOfDepartment d")
    List<ChiefOfDepartment> getAllDepartments();

    @Query("select d from  Department d where d.name =:department")
    Optional<Department> findDepartmentNyName(@Param("department") String departName);

    @Query("select s from ChiefOfDepartment s , Teacher t where s.id=t.id and  t.firstName=:firstName and t.lastName=:lastName")
    Optional<ChiefOfDepartment> findCOD(@Param("firstName") String firstName, @Param("lastName") String lastName);

    @Query("select stud from Student stud WHERE stud.group.promotion=:promotion and stud.group.semester.studyLine.type=:studyLine " +
            "and stud.group.semester.studyLine.studyLevel.type=:studyLevel and stud.group.semester.studyLine.studyLevel.specialization.name=:spec")
    List<Student> getStudentsByPromotion(@Param("spec")String spec,@Param("studyLevel")String studyLevel,@Param("studyLine")String studyLine,@Param("promotion")String promotion);

    /*and stud.group.semester.studyLine.type=:studyLine and " +
    "stud.group.semester.studyLine.studyLevel.type=:studyLevel and stud.group.semester.studyLine.studyLevel.specialization.name=:spec

    @Param("spec")String spec,@Param("studyLevel")String studyLevel,@Param("studyLine")String studyLine,
    */
}
