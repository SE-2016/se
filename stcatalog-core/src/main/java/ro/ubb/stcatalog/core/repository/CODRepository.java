package ro.ubb.stcatalog.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.person.ChiefOfDepartment;
import ro.ubb.stcatalog.core.model.person.Teacher;
import ro.ubb.stcatalog.core.repository.CatalogRepository;

import java.util.List;



@Repository
public interface CODRepository extends CatalogRepository<Teacher, Long>{

}
