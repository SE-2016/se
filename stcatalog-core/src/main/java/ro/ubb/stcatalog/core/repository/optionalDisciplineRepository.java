package ro.ubb.stcatalog.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.OptionalDiscipline;


@Repository
public interface optionalDisciplineRepository extends CatalogRepository<OptionalDiscipline, Long> {
    @Query("select COUNT(*) from OptionalDiscipline o , Semester s where o.teacher.id=:id and s.id=:semester_id and o.semester.id=:semester_id")
    int countProposed(@Param("id") long id, @Param("semester_id") long semester_id);
}
