package ro.ubb.stcatalog.core.service.Interfaces;

import ro.ubb.stcatalog.core.model.OptionalDiscipline;
import ro.ubb.stcatalog.core.model.person.Teacher;

import java.util.List;
import java.util.Optional;


public interface TeacherService {
    List<Teacher> findAll();

    List<Teacher> findAllWithGrades();
    List<OptionalDiscipline> findAllOC(long id);
    List<OptionalDiscipline> findAOCByStudent(long id);
    Optional<OptionalDiscipline> createOc(String name, String code,String specialization,String studyLevel,String studyLine, long id, long semester_id) ;
    OptionalDiscipline updateOC(long id,String discgroup);
}
