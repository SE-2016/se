package ro.ubb.stcatalog.core.service.Interfaces;

import ro.ubb.stcatalog.core.model.Discipline;

import java.util.List;


public interface DisciplineService {
    List<Discipline> findDisciplineOfStudent(Long id);
}
