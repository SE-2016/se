package ro.ubb.stcatalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.stcatalog.core.model.*;
import ro.ubb.stcatalog.core.model.person.Teacher;
import ro.ubb.stcatalog.core.repository.*;
import ro.ubb.stcatalog.core.service.Interfaces.TeacherService;

import java.util.List;
import java.util.Optional;

/**
 * Created by radu.
 */

@Service
public class TeacherServiceImpl implements TeacherService {

    private static final Logger log = LoggerFactory.getLogger(TeacherServiceImpl.class);

    @Qualifier("teacherRepository")
    @Autowired
    private TeacherRepository teacherRepository;

    @Qualifier("optionalDisciplineRepository")
    @Autowired
    private optionalDisciplineRepository optionalDisciplineRepository;

    @Qualifier("adminRepository")
    @Autowired
    private AdminRepository adminRepository;

    @Qualifier("studentRepository")
    @Autowired
    private StudentRepository studentRepository;

    @Qualifier("disciplineRepository")
    @Autowired
    private DisciplineRepository disciplineRepository;

    @Override
    @Transactional
    public List<Teacher> findAll() {
        log.trace("findAll");

        List<Teacher> teachers= teacherRepository.findAll();

        log.trace("findAll: teachers={}", teachers);

        return teachers;
    }

    @Override
    public List<Teacher> findAllWithGrades() {
        List<Teacher> teachers = teacherRepository.findAll();
        List<Discipline> disciplines = disciplineRepository.findAll();
        for (Teacher teacher : teachers) {
            int sum = 0;
            int i = 0;
            for (Discipline discipline : disciplines) {
                if (discipline.getLaboratory().getTeacher().getId() == teacher.getId()) {
                    List<Exam> exams = discipline.getExams();
                    for (Exam exam : exams) {
                        sum += Math.max(exam.getExaminationGrade(), exam.getReExaminationGrade());
                        i++;
                    }
                }
                if (discipline.getSeminary().getTeacher().getId() == teacher.getId()) {
                    List<Exam> exams = discipline.getExams();
                    for (Exam exam : exams) {
                        sum += Math.max(exam.getExaminationGrade(), exam.getReExaminationGrade());
                        i++;
                    }
                }
                if (discipline.getLecture().getTeacher().getId() == teacher.getId()) {
                    List<Exam> exams = discipline.getExams();
                    for (Exam exam : exams) {
                        sum += Math.max(exam.getExaminationGrade(), exam.getReExaminationGrade());
                        i++;
                    }
                }
            }
            if (i == 0)
                teacher.setGrade(0);
            else
                teacher.setGrade(sum / i);
        }
        return teachers;
    }

    @Override
    @Transactional
    public List<OptionalDiscipline> findAllOC(long id) {

        Department department=teacherRepository.findOne(id).getDepartment();
        List<OptionalDiscipline> optionalDisciplines= department.getOptionalDisciplines();



        return optionalDisciplines;
    }

    @Override
    @Transactional
    public List<OptionalDiscipline> findAOCByStudent(long id) {

        List<OptionalDiscipline> optionalDisciplines= studentRepository.findOne(id).getGroup().getSemester().getOptionalDisciplines();

        return optionalDisciplines;
    }

    @Override
    @Transactional
    public Optional<OptionalDiscipline> createOc(String name, String code,String specialization,String studyLevel,String studyLine, long id, long semester_id) {

        Optional<Specialization> spec=adminRepository.findSpecializationByName(specialization);
        StudyLevel studyLevel1=spec.get().getStudyLevelByType(studyLevel);
        StudyLine studyLine1=studyLevel1.getStudyLineByType(studyLine);
        Semester semester=studyLine1.getSemesterByNumber((int)semester_id);
        int count = optionalDisciplineRepository.countProposed(id, semester.getId());
        OptionalDiscipline opt = new OptionalDiscipline();
        opt.setName(name);
        opt.setCode(code);
        opt.setSemester(semester);
        Teacher teacher=teacherRepository.findOne(id);
        opt.setTeacher(teacher);
        opt.setDepartment(teacher.getDepartment());
        if (count < 2) {
            opt = optionalDisciplineRepository.save(opt);
            return Optional.of(opt);
        }
        return Optional.empty();
    }

    @Override
    @Transactional
    public OptionalDiscipline updateOC(long id, String discGroup){
        OptionalDiscipline optionalDiscipline=optionalDisciplineRepository.findOne(id);
        optionalDiscipline.setDisciplineGroup(discGroup);
        return optionalDiscipline;
    }
}
