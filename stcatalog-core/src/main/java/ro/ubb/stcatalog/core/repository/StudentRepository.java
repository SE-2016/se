package ro.ubb.stcatalog.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.Contract_OptDiscipline;
import ro.ubb.stcatalog.core.model.Discipline;
import ro.ubb.stcatalog.core.model.person.Student;

import java.util.List;
import java.util.Optional;

/**
 * Created by radu.
 */


@Repository
public interface StudentRepository extends CatalogRepository<Student, Long> {
    @Override
    @Query("select s from Student s where s.id=:user_id")
    Student findOne(@Param("user_id") Long student_id);

    @Query("select d from Discipline d inner join d.studyContracts studyContracts, StudyContract stud inner join stud.disciplines discipline,StudyContract s where studyContracts.id in s.id and " +
            "discipline.id in d.id and s.student.id=:id ")
    List<Discipline> findDisciplinesByStudentId(@Param("id") Long id);

    @Query("select d from Discipline d inner join d.students fd,Student s inner join s.disciplines failed where fd.id in d.id and failed.id in s.id and s.id=:id")
    List<Discipline> findFailedDisciplines(@Param("id") Long id);

    @Query("select c from Contract_OptDiscipline c where c.studyContract.id=:id_contract and c.optionalDiscipline.id=:id_OC")
    Optional<Contract_OptDiscipline> findCt_OC(@Param("id_contract") long id, @Param("id_OC") long id_oc);

}
