package ro.ubb.stcatalog.core.repository;

import ro.ubb.stcatalog.core.model.Discipline;

/**
 * Created by Jerry on 5/3/2016.
 */
public interface DisciplineRepository extends CatalogRepository<Discipline, Long> {
}
