package ro.ubb.stcatalog.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ro.ubb.stcatalog.core.model.person.Student;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="\"Group\"")

public class Group extends BaseEntity<Long>{

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "semester_id")
    @JsonIgnore
    private Semester semester;

    @Column(name = "groupNumber")
    private int groupNumber;

    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    @JsonIgnore
    private List<Student> students;

    @Column(name = "promotion")
    private String promotion;


    public Group() {
    }

    public Group(Long id, String promotion, int number) {
        super(id);
        this.promotion = promotion;
        this.groupNumber = number;
        students = new ArrayList<Student>();
    }

    public int getNumber() {
        return groupNumber;
    }

    public void setNumber(int number) {
        this.groupNumber = number;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }


    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }


}