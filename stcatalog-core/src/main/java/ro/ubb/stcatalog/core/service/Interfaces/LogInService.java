package ro.ubb.stcatalog.core.service.Interfaces;

import ro.ubb.stcatalog.core.model.Auxiliar;
import ro.ubb.stcatalog.core.model.person.Administer;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.model.person.Teacher;
import ro.ubb.stcatalog.core.model.person.Users;

import java.util.Optional;


public interface LogInService {
    Optional<Users> LogInStudent(String user, String pass);
    Optional<Users> LogInTeacher(String user, String pass);
    Optional<Users> LogInCod(String user, String pass);

    Optional<Users> LonInAdmin(String user, String pass);
    Student getStudent(Long id);
    Teacher getTeacher(Long id);
    Teacher getCod(Long id);

    Administer getAdmin(Long id);
    Optional<Users> logIn(String user,String pass);
    Optional<Auxiliar> getTof();

}
